#!/usr/bin/env bash

set -o errexit  # fail on simple (non-piped) error
set -o pipefail # also fail on piped commands (e.g. cat myfile.txt | grep timo)
set -o nounset  # fail when accessing unset vars

function is_container_running() {
  if [[ -n $(docker ps --quiet --filter "name=$CONTAINER_NAME") ]]; then
    return 0
  fi;
  return 1
}

function md5_dockerfile() {
  case "$OSTYPE" in
    darwin*)  md5 -q ./Dockerfile ;;
    linux*)   md5sum ./Dockerfile | awk '{ print $1 }' ;;
    *)        md5sum ./Dockerfile | awk '{ print $1 }' ;;
  esac
}

DOCKERFILE_HASH=$(md5_dockerfile)
CONTAINER_NAME=editable-cv-webserver
IMAGE_NAME=${CONTAINER_NAME}:${DOCKERFILE_HASH}

if [[ $(docker inspect --format . "${IMAGE_NAME}" 2>&1) != "." ]]; then
  echo "--- BUILDING image '${IMAGE_NAME}'---"
  docker build -t "${IMAGE_NAME}" -f Dockerfile .
fi

if is_container_running; then
  echo "--- ENTER running container '${CONTAINER_NAME}'---"
  docker exec -it ${CONTAINER_NAME} "$@"
else
  echo "--- RUN container '${CONTAINER_NAME}' $IMAGE_NAME---"
  docker run --rm -it \
    --name "${CONTAINER_NAME}" \
    --volume "$(pwd)/src":/www/data \
    --publish 8081:80 \
    "${IMAGE_NAME}" "$@"
fi
