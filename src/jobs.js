import {escape} from 'https://jspm.dev/npm:html-escaper@3.0.0!cjs';

const template = document.createElement('template');
template.innerHTML = `
  <article id="" data-is-position-most-relevant="false">
    <h3></h3>
    <div class="dates"></div>
    <p class="companyDescription" style="color: var(--color-secondary)"></p>
    <p class="jobDescription"></p>
    <p>
      <span class="technologiesUsed">
        <b>Technologies used</b>: <span></span>
        <br/>
      </span>
      <span class="nonTechSkills">
        <b>Non-Tech skills</b>: <span></span>
      </span>
    </p>
  </article>
`;

const fillNodesWithJobContent = (rootNode, job) => {
  const $article = rootNode.querySelector('article');
  $article.setAttribute('id', job.id);
  $article.dataset.isPositionMostRelevant = job.isMostRelevant;

  rootNode.querySelector('h3').innerHTML = escape(job.positions.join(', '));

  const $dates = rootNode.querySelector('.dates');
  $dates.innerHTML = escape(job.start) + ' - ' + escape(job.end);

  const $desc = rootNode.querySelector('.companyDescription');
  if (job.url) {
    const url = job.url.startsWith('http') ? job.url : `https://${job.url}`;
    $desc.innerHTML = `<a href="${url}">${escape(job.name)}</a>`;
  } else {
    $desc.innerHTML = escape(job.name);
  }
  $desc.innerHTML += ': ' + escape(job.companyDescription);

  rootNode.querySelector('.jobDescription').innerHTML = escape(job.jobDescription);
  rootNode.querySelector('.technologiesUsed span').innerHTML = escape(job.technologiesUsed);
  rootNode.querySelector('.nonTechSkills span').innerHTML = escape(job.nonTechSkills);
};

class MoreJob extends HTMLElement {
  static get observedAttributes() { return ['job-as-json-string']; }
  constructor() {
    super();
  }
  connectedCallback() {
    this.appendChild(template.content.cloneNode(true));
    this._rerender();
  }
  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'job-as-json-string') {
      this._job = JSON.parse(newValue);
      this._rerender();
    }
  }
  _rerender() {
    if (this.children.length > 0) {
      fillNodesWithJobContent(this, this._job);
    }
  }
}

customElements.define('more-job', MoreJob);
